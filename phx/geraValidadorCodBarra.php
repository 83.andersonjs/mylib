<?php

function codBarra($cod, $cnt=0)
{
    
    if($cnt>3){ return; }
    
    $i=2;
    $soma=0;
    $codModelo=11;
    
    $strCod = substr($cod, ($cnt*$codModelo), $codModelo);
    $arCod = array_reverse(str_split($strCod));

    foreach($arCod as $cd)
    {
        if($i>9){ $i=2; }
        $soma += ($cd*$i);
        $i++;
    }
    
    $calculo = ($soma % $codModelo);
    $codValidador = (($codModelo-$calculo)>9?0:(($codModelo-$calculo)==0?1:($codModelo-$calculo)));
    echo '<br>'. $strCod .'['. $codValidador .']';
    codBarra($cod,($cnt+1));

}

$cod = '85810000015.28000385213.61070121301.85800000000';
$codBarra = trim(preg_replace('/[^0-9]/', '', $cod));

echo $cod .'<hr>';
codBarra($codBarra);

?>